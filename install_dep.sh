#!/bin/bash

set -euxo pipefail

VAGRANT_VERSION="2.2.14"

wget -q "https://releases.hashicorp.com/vagrant/${VAGRANT_VERSION}/vagrant_${VAGRANT_VERSION}_x86_64.deb"
sudo apt-get install -y "./vagrant_${VAGRANT_VERSION}_x86_64.deb"
rm -f "./vagrant_${VAGRANT_VERSION}_x86_64.deb"

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

wget -q "https://www.virtualbox.org/download/oracle_vbox_2016.asc" -O- | sudo apt-key add -
wget -q "https://www.virtualbox.org/download/oracle_vbox.asc" -O- | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib"

wget -q "https://releases.hashicorp.com/packer/1.6.5/packer_1.6.5_linux_amd64.zip"
unzip *.zip
sudo mv ./packer /usr/local/bin/
sudo apt-get update -y -qq && sudo apt-get install -y -qq "linux-headers-$(uname -r)" virtualbox-6.1 > /dev/null