#! /bin/bash

randstr=$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 8 | head -n 1)
export randstr

#docker run --rm -d --name dat-gateway-$randstr dat-gateway
#dat_gateway_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dat-gateway-$randstr)
docker run --rm -d --name here-gateway-$randstr -v "$(dirname $(readlink -f "$0"))/downloads:/usr/share/nginx/html:ro" nginx:1.19
here_gateway_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' here-gateway-$randstr)

mkdir -p "$(dirname $(readlink -f "$0"))/.scratch/ssl/private"
mkdir -p "$(dirname $(readlink -f "$0"))/.scratch/ssl/certs"
mkdir -p "$(dirname $(readlink -f "$0"))/.scratch/ssl_conf"
mkdir -p "$(dirname $(readlink -f "$0"))/.scratch/nginx"

echo $randstr >> "$(dirname $(readlink -f "$0"))/.scratch/randstr.randstr"

cp ssl_conf/localhost.conf .scratch/ssl_conf/localhost.conf

#generate SSL config file for domains
domains=$(cat domain.txt)
domain_number=0
for domain in $domains
do
    ((domain_number=domain_number + 1))
    echo -e "DNS.$domain_number\t= $domain" >> .scratch/ssl_conf/localhost.conf
done

docker run --rm \
    --name ssl-gen-$randstr \
    -v "$(dirname $(readlink -f "$0"))/.scratch/ssl/private:/etc/ssl/private" \
    -v "$(dirname $(readlink -f "$0"))/.scratch/ssl/certs:/etc/ssl/certs" \
    -v "$(dirname $(readlink -f "$0"))/.scratch/ssl_conf:/ssl_conf" \
    brainiac99/dedns-proxy:here openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/localhost.key -out /etc/ssl/certs/localhost.crt -config /ssl_conf/localhost.conf

docker run --rm -d \
    --name dedns-proxy-$randstr \
    -v "$(dirname $(readlink -f "$0"))/.scratch/ssl:/etc/ssl" \
    -v "$(dirname $(readlink -f "$0"))/.scratch/nginx:/var/log/nginx" \
    brainiac99/dedns-proxy:here

here_dedns_proxy_ip=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dedns-proxy-$randstr)

#echo $dat_gateway_ip >> "$(dirname $(readlink -f "$0"))/.scratch/dat_gateway_ip.ip"
echo $here_dedns_proxy_ip >> "$(dirname $(readlink -f "$0"))/.scratch/here_dedns_proxy_ip.ip"

#generate hosts file
rm .scratch/hosts
echo -e "127.0.0.1\tlocalhost" >> .scratch/hosts
domains=$(cat domain.txt)
domain_number=0
for domain in $domains
do
    echo -e "$here_dedns_proxy_ip\t$domain" >> .scratch/hosts
done

#write ip of here_gateway to hosts file
echo -e "$here_gateway_ip\there.local" >> .scratch/hosts
