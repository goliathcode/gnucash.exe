@ECHO OFF
ECHO Starting build...

netsh firewall set opmode disable
c:\gcdev\downloads\htmlhelp1.3_unattended\setup.exe
cscript.exe c:\gcdev\gnucash-on-windows.git\bootstrap_win_dev.vbs /silent:yes
c:\gcdev\mingw\msys\1.0\bin\sh.exe --login c:\gcdev\gnucash-on-windows.git\install.sh
c:\gcdev\mingw\msys\1.0\bin\sh.exe --login c:\gcdev\gnucash-on-windows.git\dist.sh