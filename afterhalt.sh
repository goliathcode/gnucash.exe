#! /bin/bash

randstr_file="$(dirname $(readlink -f "$0"))/.scratch/randstr.randstr"
randstr=$(cat "$randstr_file")

docker stop here-gateway-$randstr
docker stop dedns-proxy-$randstr

rm -rf "$(dirname $(readlink -f "$0"))/.scratch"