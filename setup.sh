
#! /bin/bash

url=$(git remote get-url origin)
dirurl=$(dirname "$url")
basename "$url"; basename -s .git "$url";

git clone -b 2.1000 --single-branch "$dirurl"/gnucash-on-windows.git gnucash-on-windows.git
#pushd gnucash-on-windows.git && git checkout f807b4f534a8299c5ce0f22b09991ab9b2d7d46a && popd

git clone -b master "$dirurl"/Gnucash.git Gnucash.git

git clone -b release-1.8.1 https://github.com/google/googletest gtest
#pushd gtest && git checkout 3ff1e8b98a3d1d3abc24a5bacb7651c9b32faedd && popd

git clone -b 2.6 --single-branch https://github.com/Gnucash/gnucash-docs.git gnucash-docs/repos

cp gnucash-on-windows.git/custom.sh.sample gnucash-on-windows.git/custom.sh
echo 'GWENHYWFAR_URL="https://www.aquamaniac.de/sites/download/gwenhywfar-4.17.0.tar.gz"' >> gnucash-on-windows.git/custom.sh
echo 'AQBANKING_URL="https://www.aquamaniac.de/sites/download/aqbanking-5.6.12.tar.gz"' >> gnucash-on-windows.git/custom.sh

#htmlhelp.exe has to be changed to github.com here because *.microsoft.com cannot be redirected using hosts file
echo 'HH_URL="https://github.com/htmlhelp.exe"' >> gnucash-on-windows.git/custom.sh
echo 'DOCS_SCM_REV="2.6"' >> gnucash-on-windows.git/custom.sh
echo 'GNUCASH_SCM_REV="main"' >> gnucash-on-windows.git/custom.sh
echo 'UPDATE_DOCS=no' >> gnucash-on-windows.git/custom.sh

#Set release tag if this commit has a tag.
pushd Gnucash.git && tag=$(git tag --points-at HEAD) && popd
if [ $tag ]; then
    echo 'RELEASE_TAG="'${tag}'"' >> gnucash-on-windows.git/custom.sh
    echo 'We will build release' ${tag}
fi

mkdir -p downloads
pushd downloads && wget -nv -nc -i ../url.txt && popd
#pushd downloads && wget -nv -nc --content-disposition --trust-server-names -i ../.scratch/nginx/access.log && popd

if [[ ! -d "downloads/htmlhelp1.3_unattended" ]]
then
    unzip downloads/htmlhelp1.3_unattended.zip -d downloads/htmlhelp1.3_unattended
fi
